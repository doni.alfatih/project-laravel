<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create (){
        return view ('cast.create');
    }

    public function peranfilm(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }

    public function index(){
        $cast_list = DB::table('cast')->get();

        return view ('cast.index', compact('cast_list'));
    }

    public function show($id){
        $cast_list = DB::table('cast')->where('id', $id)->first();

        return view ('cast.show', compact('cast_list'));
    }

    
    public function edit($id){
        $cast_list = DB::table('cast')->where('id', $id)->first();

        return view ('cast.edit', compact('cast_list'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $affected = DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('/cast');    
    }

    public function delete($id){
        DB::table('cast')->where('id', $id)->delete();
        
        return redirect('/cast');
    }
}
