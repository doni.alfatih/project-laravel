<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view ('latihan laravel.register');
    }

    public function sign(Request $request){
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        return view ('latihan laravel.welcome',compact('firstname','lastname'));
    }
}
