@extends('master')

@section('judul')
Buat Account Baru!
@endsection

@section('konten')
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">  
    @csrf 
    <p>First Name :</p> <form><input type="text" name="firstname"/>
    <p>Last Name :</p> <form><input type="text" name="lastname"/>
        
    <p>Gender :</p> 
        <input type="radio" name="pria" value="Male"/> Male
       <br> <input type="radio" name="wanita" value="Female"/> Female <br/>
        <input type="radio" name="lainnya" value="Other"/> Other

    <p>Nationality :</p>
    <select name="negara" >
       <option value="Indonesia"> Indonesia</option>
       <option value="Amerika">Amerika</option>
       <option value="Inggris" >Inggris</option>
   </select>

    <p>Language Spoken :</p>
        <input type="checkbox" name="bahasaindonesia" /> Bahasa Indonesia
       <br> <input type="checkbox" name="bahasaenglish" /> English <br/>
        <input type="checkbox" name="bahasaother" /> Other

    <p>Bio : </p>
    <textarea name="komentar" rows="5" cols="20"></textarea>
 
    <br/>
    
    <input type="submit" value="Sign Up">
    
</form> 
@endsection
    