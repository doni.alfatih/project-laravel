@extends('master')

@section('judul')
Detail Cast {{$cast_list->nama}}
@endsection

@section('konten')

<h1>{{$cast_list->nama}}</h1>
<p>{{$cast_list->umur}}</p>
<p>{{$cast_list->bio}}</p>
@endsection